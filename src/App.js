import React, { useState } from "react";
import { Routes, Route } from "react-router-dom";

import Products from "./components/product/Products";
import Header from "./components/header/Header";
import Modal from "./components/modal/Modal";
import Button from "./components/button/Button";
import Cart from "./components/cart/Cart";
import Favorites from "./components/favorites/Favorites";

import './App.scss';


const App = () => {

    const [isModalActive, setIsModalActive] = useState(false)
    const [productModal, setProductModal] = useState({})
    const [cart, setCart] = useState(JSON.parse(localStorage.getItem('cart')) || [])
    const [liked, setLiked] = useState(JSON.parse(localStorage.getItem('liked')) || [])

    const activateModal = (product) => {
        setIsModalActive(true)
        setProductModal(product)
    }


    const deActivateModal = () => { setIsModalActive(false) }


    const addProductToCart = (product) => {

        const prevCartStore = JSON.parse(localStorage.getItem('cart'))

        if (prevCartStore) {

            const newCartStore = [...prevCartStore]

            newCartStore.map(el => { return el.id === product.id ? el.quantity++ : false })
            const isProductInCart = newCartStore.find(el => el.id === product.id)

            if (!isProductInCart) {
                product.quantity = 1
                newCartStore.push(product)
            }

            localStorage.setItem('cart', JSON.stringify([...newCartStore]))

        } else {

            const newProduct = Object.assign({}, product)
            newProduct.quantity = 1
            localStorage.setItem('cart', JSON.stringify([newProduct]))

        }

        setCart(JSON.parse(localStorage.getItem('cart')))

        deActivateModal()

    }


    const deleteProductFromCart = (product) => {

        const newCart = cart.filter(el => el.id !== product.id)

        localStorage.setItem('cart', JSON.stringify(newCart))
        setCart(newCart)

    }


    const toggleLiked = (product, isProductLiked) => {

        let toggleLiked = []

        if (isProductLiked) {
            toggleLiked = Array.from(new Set([...liked])).filter(el => el.id !== product.id)
        } else {
            toggleLiked = Array.from(new Set([...liked, product]))
        }

        localStorage.setItem('liked', JSON.stringify(toggleLiked))
        setLiked(toggleLiked)

    }

      return (
          <>

              <Header cart={cart} liked={liked}/>

              <Routes>

                  <Route path="/" element={
                      <Products
                          liked={liked}
                          activateModal={activateModal}
                          toggleLiked={toggleLiked}
                      />
                  } />

                  <Route path="/cart" element={
                      <Cart
                          cart={cart}
                          deleteProduct={deleteProductFromCart}
                      />
                  }/>

                  <Route path="/favorites" element={
                      <Favorites
                          favorites={liked}
                          toggleLiked={toggleLiked}
                          activateModal={activateModal}
                      />
                  }/>

                  <Route path="*" element={
                      <div>
                          <div style={{
                              backgroundImage: "public/notfound.png"
                          }}
                          />
                          <h2>Something went wrong :((((</h2>
                      </div>
                  }/>


              </Routes>

              { isModalActive && <Modal
                  header={`Buy product ${productModal.code}?`}
                  text={`Do you really want to purchase ${productModal.name}?`}
                  onModalClose={deActivateModal}
                  actions={
                      <div className="modalButtons">
                          <Button
                              text="Add"
                              onClick={() => addProductToCart(productModal)}/>
                          <Button
                              text="Cancel"
                              onClick={deActivateModal}/>
                      </div>
                  }

              />}
          </>
     );
}

export default App;
