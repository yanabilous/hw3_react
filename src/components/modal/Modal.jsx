import React from 'react';
import PropTypes from 'prop-types';
import { faSquareXmark } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import './Modal.scss';


const Modal = (props) => {

        return (
            <div className="modalBackdrop" onClick={props.onModalClose}>
                <div className="modalWrapper" onClick={(ev) => ev.stopPropagation()}>
                    <div className="modalHeader">
                        <h4>{props.text}</h4>
                        <FontAwesomeIcon
                            icon={ faSquareXmark }
                            onClick={props.onModalClose}
                        />
                    </div>
                    <span className="modalText">{props.header}</span>
                    <div className="modalButtons">
                        {props.actions}
                    </div>
                </div>
            </div>
        )
}

export default Modal;

// Modal.propTypes = {
//     header: PropTypes.string.isRequired,
//     text: PropTypes.string.isRequired,
//     onModalClose: PropTypes.func.isRequired,
//     actions: PropTypes.element.isRequired
// }
