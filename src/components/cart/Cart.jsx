import React, { useState } from 'react';

import CartItem from './CartItem';
import Button from '../button/Button';
import Modal from "../modal/Modal";

import './Cart.scss'


const Cart = (props) => {

    const [isModalActive, setIsModalActive] = useState(false)
    const [productModal, setProductModal] = useState({})


    return(
        <>
            <div className="cart">
                {props.cart.map(cartItem => (
                    <CartItem
                        item={cartItem}
                        key={cartItem.id}
                        deleteProduct={() => {
                            setIsModalActive(true)
                            setProductModal(cartItem)
                        }}
                    />))
                }
                <div className="cartTotal">
                    <div className="cartTotalAmount">

                    </div>
                    <Button
                        text="SUBMIT ORDER"
                        onClick={() => console.log('Your order nested!')}
                    />
                </div>
            </div>


            { isModalActive && <Modal
                header={`Do you really want to delete product ${productModal.code} from cart?`}
                text={`Clear ${productModal.name} from cart?`}
                onModalClose={() => setIsModalActive(false)}
                actions={
                    <div className="modalButtons">
                        <Button
                            text="Delete"
                            onClick={() => {
                                props.deleteProduct(productModal)
                                setIsModalActive(false)
                            }}/>
                        <Button
                            text="Cancel"
                            onClick={() => setIsModalActive(false)}/>
                    </div>
                }

            />}


        </>
    )
}

export default Cart;