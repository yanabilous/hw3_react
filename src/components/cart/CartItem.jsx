
import React from 'react';

import './CartItem.scss'
import Button from "../button/Button";

const CartItem = (props) => {

    return (

        <div className="cartItem">
            <div className="cartItemImage"
                 style={{
                     backgroundImage: `url(${props.item.imageUrl})`
                 }}
            />
            <p className="cartItemName">{props.item.name}</p>
            <div className="cartItemPricing">
                <span className="cartItemPricingQuantity">{props.item.quantity}</span>
                <span className="cartItemPricingPrice">{props.item.price}</span>
                <span className="cartItemPricingTotal">{props.item.quantity * props.item.price}</span>
                <Button text="Delete item" onClick={() => {props.deleteProduct(props.item)}} />
            </div>
        </div>
    )
}

export default CartItem;