import React from 'react';
import PropTypes from 'prop-types';
import './Button.scss';

const Button = (props) => {

        return(
            <button
                type='button'
                onClick={props.onClick}
            >{props.text}</button>
        )
}

export default Button;


Button.propTypes = {
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
}