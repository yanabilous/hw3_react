import React from 'react';

import ProductCard from "../productCard/ProductCard";

import './Favorites.scss'



const Favorites = (props) => {

    return(
        <>
            <div className='productsList'>
                {props.favorites.map(el => <ProductCard
                    product={el}
                    key={el.id}
                    activateModal={props.activateModal}
                    toggleLiked={props.toggleLiked}
                    liked={props.favorites.map(liked => liked.id).includes(el.id)}
                />)}
            </div>

        </>
    )
}

export default Favorites;