import './Products.scss';
import ProductCard from "../productCard/ProductCard";
import React, {useEffect, useState} from "react";
import PropTypes from "prop-types";

const Products = (props) => {

    const [products, setProducts] = useState([]);

    useEffect(() => {

        fetch('data.json')
            .then(response => response.json())
            .then(data => setProducts(data))

    })

        return (
            <div className='productsList'>
                {products.map(el => <ProductCard
                    product={el}
                    key={el.id}
                    activateModal={props.activateModal}
                    toggleLiked={props.toggleLiked}
                    liked={props.liked.map(liked => liked.id).includes(el.id)}
                />)}
            </div>
        );
}

export default Products;

// Products.propTypes = {
//     activateModal: PropTypes.func.isRequired,
//     toggleLiked: PropTypes.func.isRequired,
//     liked: PropTypes.array
// }
//
// Products.defaultProps = {
//     liked: []
// }
